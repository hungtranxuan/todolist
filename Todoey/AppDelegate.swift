//
//  AppDelegate.swift
//  Todoey
//
//  Created by Robert Travis on 3/5/19.
//  Copyright © 2019 Hung Tran Xuan. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //print(Realm.Configuration.defaultConfiguration.fileURL)
        
       
        do {
            _ = try Realm()
        } catch {
            print("Error inintialising realm, \(error)")
        }
        
        
        
        return true
    }
}
