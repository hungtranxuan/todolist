//
//  Category.swift
//  Todoey
//
//  Created by Robert Travis on 3/6/19.
//  Copyright © 2019 Hung Tran Xuan. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var colour: String = ""
    let items = List<Item>()
}
